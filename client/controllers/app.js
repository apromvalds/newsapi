var app = angular.module('home', ['ngRoute', 'ngAnimate']);

app.constant('API_URL','http://localhost');

app.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl : 'pages/home.html',
            controller  : 'HomeController'
        })

        .when('/post', {
            templateUrl : 'pages/post.html',
            controller :  'postnewController'
        })

        .when('/get', {
            templateUrl : 'pages/get.html',
            controller  : 'getarticlesController'
        })

        .when('/delete', {
            templateUrl : 'pages/delete.html',
            controller  : 'deletearticlesController'
        })

        .when('/edit', {
            templateUrl : 'pages/edit.html',
            controller  : 'editController'
        })

        .otherwise({redirectTo: '/'});

});


app.controller('postnewController', function($scope, $http, API_URL) {
    $scope.url = API_URL;
    $scope.postArticle = function() {
        $http({
            method: 'POST',
            url: $scope.url + '/article/',
            data: '{name:'+ $scope.articleName +' }'
        }).then(function successCallback() {
            $scope.results = 'posted';
        }, function errorCallback() {
            $scope.results = 'not posted';
        });
    };
});

app.controller('editController', function($scope, $http, API_URL) {
    $scope.url = API_URL;
    $scope.editArticle = function() {
        $http({
            method: 'PUT',
            url: $scope.url + '/article/',
            data: '{id: '+ $scope.articleId +', name:'+ $scope.articleName +' }'
        }).then(function successCallback() {
            $scope.results = 'posted';
        }, function errorCallback() {
            $scope.results = 'not posted';
        });
    };
});

app.controller('getarticlesController', function ($scope, $http, API_URL) {
    $scope.url = API_URL;
    $scope.idValue = '';

    $scope.getArticle = function () {

        $http({
            method: 'GET',
            url: $scope.url + '/article/' + $scope.idValue
        }).then(function successCallback(response) {
            $scope.getArticles = response.data[0];

        }, function errorCallback(response) {
            $scope.articleError = "No article with this ID"
        });
    };

    $scope.$watch('getArticles', function() {
        console.log($scope.getArticles);
    });
});

app.controller('deletearticlesController', function ($scope, $http, API_URL) {
    $scope.url = API_URL;
    $scope.idValue = '';

    $scope.getArticle = function () {

        $http({
            method: 'DELETE',
            url: $scope.url + '/article/' + $scope.idValue
        }).then(function successCallback(response) {
            $scope.articleError = 'Article deleted';
        }, function errorCallback(response) {
            $scope.articleError = "No article with this ID"
        });
    };
});

