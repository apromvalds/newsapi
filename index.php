<?php
/**
 * News
 * @version 1.0.0
 */

require_once __DIR__ . '/vendor/autoload.php';
$settings = require __DIR__ . '/settings.php';
$app = new Slim\App($settings);


$container = $app->getContainer();

$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'],
        $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};


/**
 * GET getarticleById
 * Summary: Find article by ID
 * Notes: Returns an article
 * Output-Formats: [application/xml, application/json]
 */
$app->GET('/article/{articleId}', function ($request, $response, $args) {



    $id = $args['articleId'];
    $responseValue = $response->withJson($this->db->query("SELECT * from news where id = $id")->fetchAll());


    return $responseValue;

    /*if (  ){
        return $response->withStatus(404)->write('Article not found');
    } else {
        return $responseValue;
    }*/


});


/**
 * POST addArticle
 * Summary: Add a new article
 * Notes:
 * Output-Formats: [application/xml, application/json]
 */
$app->POST('/article', function ($request, $response, $args) {


    $json = $request->getBody();
    $body = json_decode($json, true);
    $title = addslashes($body['title']);
    $category = addslashes($body['category']);

    return $response->withJson($this->db->query("insert into  news (title, category) values ('$title', '$category')"));

});


/**
 * DELETE deletearticle
 * Summary: Delete an article
 * Notes:
 * Output-Formats: [application/xml, application/json]
 */
$app->DELETE('/article/{articleId}', function ($request, $response, $args) {

    $id = $args['articleId'];
    return $response->withJson($this->db->query("DELETE from news where id = $id"));
});


/**
 * PUT updateArticle
 * Summary: Update an article
 * Notes:
 * Output-Formats: [application/xml, application/json]
 */
$app->PUT('/article', function ($request, $response, $args) {
    $json = $request->getBody();
    $body = json_decode($json, true);

    $title = addslashes($body['title']);
    $category = addslashes($body['category']);
    $id = addslashes($body['id']);

    return $response->withJson($this->db->query("UPDATE  news  SET title = '$title', category = '$category' WHERE id = $id"));
});


$app->run();
